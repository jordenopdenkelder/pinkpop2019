function rowHighlight() {

    var index,
        table = document.getElementById("pinkHighlightRow");

    for(var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function()
        {
            if(typeof index !== "undefined")
            {
                table.rows[index].classList.toggle("unselected");
            }

            console.log(typeof index);
            index = this.rowIndex;
            this.classList.toggle("selected");
            console.log(typeof index);
        };

    }
}
rowHighlight();

function dotsTextExpander() {
    var dotsText = document.getElementById("dots");
    var showText = document.getElementById("underDots");
    var dotsButton = document.getElementById("clickMe");

    if (dotsText.style.display === "none") {
        dotsText.style.display = "inline";
        dotsButton.innerHTML = "...";
        showText.style.display = "none";
    } else {
        dotsText.style.display = "none";
        dotsButton.innerHTML = "...";
        showText.style.display = "inline";

    }

}


