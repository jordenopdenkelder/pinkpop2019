class Album {
    constructor(artiest, album, tracks, dur) {
        this.artiest = artiest;
        this.album = album;
        this.tracks = tracks;
        this.dur = dur;
    }
    get duration(){
        {

        }
    }


}

class Track {
    constructor(url, track, duration)
    {
        this.url = url;
        this.track = track;
        this.duration = duration;
    }

    get seconds() {
        let mins = Math.floor(this.duration / 60);
        let secs = Math.floor(this.duration % 60);
        let output = mins.toString().padStart(2, '0') + ':' + secs.toString().padStart(2, '0');
        return output
    }

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

window.onload = loadAlbums();
function loadAlbums() {
    var REQA = new XMLHttpRequest();
    REQA.overrideMimeType("application/json");
    REQA.open('GET', 'jukeboxArrayAlbumAndTracks.json', true);
    REQA.onreadystatechange = function () {
        if (REQA.readyState === 4 && REQA.status === 200) {
            var albumsParse = JSON.parse(REQA.responseText);
            var kissAlbum =  Object.assign(new Album, albumsParse.albumsList[0]);
            console.log(kissAlbum)
            for(var countUp=0; countUp<albumsParse.albumsList.length;countUp++){
                selBand.innerHTML = selBand.innerHTML +
                    '<option value='+countUp+'>' + albumsParse.albumsList[countUp].artiest+ ": " + albumsParse.albumsList[countUp].album+ " - Duration: " +albumsParse.albumsList[countUp].dur+ '</option>';

            }
        }
    };
    return REQA.send(null);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function selectedBandTracks(){
    var selectedArtiestAndAlbum = document.getElementById('selBand');
    var userChoose = selectedArtiestAndAlbum.options[selectedArtiestAndAlbum.selectedIndex].value;
    var REQT = new XMLHttpRequest();
    REQT.overrideMimeType("application/json");
    REQT.open('GET', 'jukeboxArrayAlbumAndTracks.json', true);
    REQT.onreadystatechange = function () {
        if (REQT.readyState === 4 && REQT.status === 200){
            var tracksParse = JSON.parse(REQT.responseText);
            if(userChoose =='0'){
                var justDur =  Object.assign(new Track, tracksParse.tracksList[0]);
                var howDur =  Object.assign(new Track, tracksParse.tracksList[1]);
                selTrack.innerHTML =
                    '<option value=>' +'Kies de track die je wilt luisteren.'+ '</option>'+
                    '<option value= "song1" >' + tracksParse.tracksList[0].track + ' - Duration: ' + justDur.seconds + '</option>'+
                    '<option value= "song2" >' + tracksParse.tracksList[1].track + ' - Duration: ' + howDur.seconds + '</option>';
            } if(userChoose == '1'){
                var overDur =  Object.assign(new Track, tracksParse.tracksList[2]);
                var runDur =  Object.assign(new Track, tracksParse.tracksList[3]);
                selTrack.innerHTML =
                    '<option value=>' +'Kies de track die je wilt luisteren.'+ '</option>'+
                    '<option value= "song3" >' + tracksParse.tracksList[2].track + ' - Duration: ' + overDur.seconds + '</option>'+
                    '<option value= "song4" >' + tracksParse.tracksList[3].track + ' - Duration: ' + runDur.seconds + '</option>';
                console.log(selTrack)
            }if(userChoose =='2'){
                var beatDur =  Object.assign(new Track, tracksParse.tracksList[4]);
                var ninDur =  Object.assign(new Track, tracksParse.tracksList[5]);
                selTrack.innerHTML =
                    '<option value=>' +'Kies de track die je wilt luisteren.'+ '</option>'+
                    '<option value= "song5" >' + tracksParse.tracksList[4].track + ' - Duration: ' + beatDur.seconds + '</option>'+
                    '<option value= "song6" >' + tracksParse.tracksList[5].track + ' - Duration: ' + ninDur.seconds + '</option>';
                console.log(selTrack)
            }if(userChoose == 'default'){
                selTrack.innerHTML= '<option value=>' + "Kies de track die je wilt beluisteren" +  '</option>';
                console.log(selTrack)
            }
        }
    };
    return REQT.send(null);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function urlPlayer() {
    var videoSel = document.getElementById('selTrack');
    var videoPlay = videoSel.options[videoSel.selectedIndex].value;
    var REQV = new XMLHttpRequest();
    REQV.overrideMimeType("application/json");
    REQV.open('GET', 'jukeboxArrayAlbumAndTracks.json', true);
    REQV.onreadystatechange = function () {
        if (REQV.readyState === 4 && REQV.status === 200) {
            var videoParse = JSON.parse(REQV.responseText);
            if (videoPlay == "song1") {
                document.getElementById("playerTube").src = videoParse.tracksList[0].url;
                console.log("Just Like Heaven")
            }if (videoPlay == "song2") {
                document.getElementById("playerTube").src = videoParse.tracksList[1].url;
                console.log("How Beautiful You Are")
            }if (videoPlay == "song3") {
                document.getElementById("playerTube").src = videoParse.tracksList[2].url;
                console.log("It Ain't Over Til It's Over")
            }if (videoPlay == "song4") {
                document.getElementById("playerTube").src = videoParse.tracksList[3].url;
                console.log("Always On The Run")
            }if (videoPlay == "song5") {
                document.getElementById("playerTube").src = videoParse.tracksList[4].url;
                console.log("Baby on fire")
            }if (videoPlay == "song6") {
                document.getElementById("playerTube").src = videoParse.tracksList[5].url;
                console.log("Naaiers")
            }
        }
    };
    return REQV.send(null);
}